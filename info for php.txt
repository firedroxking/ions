The War of Emperium is a guild versus guild enabled battle that happens every [DAYS AND HOURS]. 
In War of Emperium (WoE for short) guilds try to take over castles and defend others from taking over the castle they've taken,
to take the castle they should kill the enemys Emperium, a large, golden stone located deep within the castle. 
To successfully conquer a guild castle, a player must break that castle's Emperium. 
It is the defending guild's responsibility to make sure that enemies do not make it that far! 
When the Emperium is broken, the castle ownership is transferred to the guild of the player that broke it. 
A message will display on screen in yellow global text (visible by all online players).
At this time, all players other than that player and his guildmates are immediately warped out of the fort and back to their save point.