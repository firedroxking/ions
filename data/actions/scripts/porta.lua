local level = 10
function onUse(cid, fromPos, toPos)
    return getPlayerLevel(cid) <= level and doTeleportThing(cid, toPos) or doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, 'Somente jogadores de nível '..level..' ou inferior podem entrar.') and false
end