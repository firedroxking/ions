local idAlavancaEscada         = 65000 -- {x = 32227, y = 32278, z = 8}
local idEscadaTerra            = 65001 -- {x = 32226, y = 32276, z = 8}
local idAlavancaTeletransporte = 65004 -- {x = 32225, y = 32285, z = 10}

local teletransporteLocal   = {x = 32232, y = 32276, z =  9}
local teletransporteDestino = {x = 32225, y = 32275, z = 10}

-- ACTIONS.XML
function onUse(playerID, item, fromPosition, itemUsed, toPosition)
	-- DEGUG
	-- doPlayerSendTextMessage(playerID, 22, item.uid .. " - " .. item.itemid)

	-- PARTE 01 : PLAYER ENCONTRAR E ATIVAR ALAVANCA
    if(item.uid == idAlavancaEscada) then
    	if(item.itemid == 1945) then    		
			doTransformItem(idEscadaTerra, 369)
		elseif(item.itemid == 1946) then
			doTransformItem(idEscadaTerra, 351)
		end
	end

	-- PARTE 03 : ATIVAR O TELETRANSPORTE
    if(item.uid == idAlavancaTeletransporte) then
    	if(item.itemid == 1945) then
    		teleport = doCreateTeleport(1387, teletransporteDestino, teletransporteLocal)
		elseif(item.itemid == 1946) then
			doRemoveItem(getTileItemById(teletransporteLocal, 1387).uid, 1)
		end
	end
end