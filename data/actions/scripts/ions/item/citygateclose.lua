local GATES =
{
	-- COULLIST SOUTH-WEST GATE
	{ ["UNDER"] = {x = 85, y = 70, z = 7}, ["ABOVE"] = {x = 85, y = 70, z = 6} },
	{ ["UNDER"] = {x = 86, y = 70, z = 7}, ["ABOVE"] = {x = 86, y = 70, z = 6} },
	{ ["UNDER"] = {x = 87, y = 70, z = 7}, ["ABOVE"] = {x = 87, y = 70, z = 6} },
	{ ["UNDER"] = {x = 88, y = 70, z = 7}, ["ABOVE"] = {x = 88, y = 70, z = 6} },
	{ ["UNDER"] = {x = 89, y = 70, z = 7}, ["ABOVE"] = {x = 89, y = 70, z = 6} },
	{ ["UNDER"] = {x = 90, y = 70, z = 7}, ["ABOVE"] = {x = 90, y = 70, z = 6} },
	{ ["UNDER"] = {x = 91, y = 70, z = 7}, ["ABOVE"] = {x = 91, y = 70, z = 6} },
	
	-- COULLIST NORTH-EAST GATE
	{ ["UNDER"] = {x = 129, y = 42, z = 7} },
	{ ["UNDER"] = {x = 130, y = 42, z = 7} },
	{ ["UNDER"] = {x = 131, y = 42, z = 7} },
	{ ["UNDER"] = {x = 132, y = 42, z = 7} },
	{ ["UNDER"] = {x = 133, y = 42, z = 7} }	
}

-- ACTIONS.XML
function onUse(playerID, item, fromPosition, itemUsed, toPosition)
	local idMechanism = 9531

	-- DEGUG
	-- doPlayerSendTextMessage(playerID, 22, item.uid .. " - " .. item.itemid)

	-- TODO: COLOCAR POR TEMPO, MECANISMO ATIVAR A NOITE E DESATIVAR DE MANHA
    if(item.itemid == idMechanism) then
    	for index=1, table.getn(GATES), 1 do
	    	local gateUnder = GATES[index]["UNDER"]
	    	local gateAbove = GATES[index]["ABOVE"]

	    	-- QUANDO O PORTAO NAO TIVER A PARTE DE CIMA
	    	if(gateAbove == nil and getTileItemById(gateUnder, 1547).uid == 0) then
		    	doCreateItem(1547, 1, gateUnder)
		    elseif(gateAbove == nil and getTileItemById(gateUnder, 1547).uid ~= 0) then
		    	doRemoveItem(getTileItemById(gateUnder, 1547).uid, 1)
		    end

		    -- QUANDO O PORTAO TIVER A PARTE DE CIMA
	    	if(gateAbove ~= nil and getTileItemById(gateAbove, 1547).uid ~= 0) then
		    	doRemoveItem(getTileItemById(gateAbove, 1547).uid, 1)
		    	doCreateItem(1547, 1, gateUnder)
		    elseif(gateAbove ~= nil and getTileItemById(gateUnder, 1547).uid ~= 0) then
		    	doRemoveItem(getTileItemById(gateUnder, 1547).uid, 1)
		    	doCreateItem(1547, 1, gateAbove)
		    end
	    end
	end
end