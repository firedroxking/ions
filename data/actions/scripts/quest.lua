function onUse(cid, item, frompos, item2, topos)

       if item.uid == 1670 then
           queststatus = getPlayerStorageValue(cid,1670)
           if queststatus == -1 then
               doPlayerSendTextMessage(cid,22,"You have found a Libera Star.")
               doPlayerAddItem(cid,7366,1)
               setPlayerStorageValue(cid,1670,1)
           else
               doPlayerSendTextMessage(cid,22,"It is empty.")
           end
       elseif item.uid == 1671 then
           queststatus = getPlayerStorageValue(cid,1670)
           if queststatus == -1 then
               doPlayerSendTextMessage(cid,22,"You have found a Libera Wand.")
               doPlayerAddItem(cid,7735,1)
               setPlayerStorageValue(cid,1670,1)
           else
               doPlayerSendTextMessage(cid,22,"It is empty.")
           end
       elseif item.uid == 1672 then
           queststatus = getPlayerStorageValue(cid,1670)
           if queststatus == -1 then
               doPlayerSendTextMessage(cid,22,"You have found a Quest wand.")
               doPlayerAddItem(cid,7958,1)
               setPlayerStorageValue(cid,1670,1)
           else
               doPlayerSendTextMessage(cid,22,"It is empty.")
           end
       elseif item.uid == 1673 then
           queststatus = getPlayerStorageValue(cid,1670)
           if queststatus == -1 then
               doPlayerSendTextMessage(cid,22,"You have found a Libera Sword.")
               doPlayerAddItem(cid,8931,1)
               setPlayerStorageValue(cid,1670,1)
           else
               doPlayerSendTextMessage(cid,22,"It is empty.")
           end
       elseif item.uid == 1674 then
           queststatus = getPlayerStorageValue(cid,1670)
           if queststatus == -1 then
               doPlayerSendTextMessage(cid,22,"You have found a Libera Hammer.")
               doPlayerAddItem(cid,7450,1)
               setPlayerStorageValue(cid,1670,1)
           else
               doPlayerSendTextMessage(cid,22,"It is empty.")
           end
       elseif item.uid == 1675 then
           queststatus = getPlayerStorageValue(cid,1670)
           if queststatus == -1 then
               doPlayerSendTextMessage(cid,22,"You have found a Libera Axe.")
               doPlayerAddItem(cid,7455,1)
               setPlayerStorageValue(cid,1670,1)
           else
               doPlayerSendTextMessage(cid,22,"It is empty.")
           end

    else
        return 0
       end

       return 1
end