        shp_id = 7588 -- Item a ser vendido
        backpackshp_id = 2000 -- Backpack
		custoshp_id = 6000 -- Valor
		cargasshp_id = 5 -- Cargas

local name = getItemNameById(shp_id)
----- End Config -----
function onUse(cid, item, fromPosition, itemEx, toPosition)
        if doPlayerRemoveMoney(cid, custoshp_id) == TRUE then
                local bag = doPlayerAddItem(cid)
                        doSendAnimatedText(fromPosition, "Purchased", TEXTCOLOR_YELLOW)
                        doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have purchased a backpack of ".. name .."s for ".. custoshp_id .." gold.")
						for i=1,20 do
                        doPlayerAddItem(cid, shp_id, cargasshp_id)
                end
                else
                        doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "You need ".. custoshp_id .." gold coins for a backpack of ".. name .."s.")
                end
        return FALSE
end