local config = {
     remove_on_use = false, -- Remover
     min_heal = 5, -- Mínimo
     max_heal = 10 -- Máximo
}
function onUse(cid, item)
     doCreatureAddHealth(cid, math.random(config.min_heal, config.max_heal))
     doCreatureSay(cid, "Aaaah...", 19)
	
        
     if (config.remove_on_use) then
             doRemoveItem(item.uid, 1)
     end
        
     return true
end