local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local thinkMsg = {
	"Se voce precisa de cartas ou pacotes, venha falar comigo. Eu posso explicar tudo.",
	"Hey, envie uma carta para um amigo. Mantenha-se sempre em contato.",
	"Nao, nao, nao. Nao tem nenhum pacote 'bug', estou te dizendo!",
	"Bem vindo ao correio oficial!"
}

function onCreatureAppear(cid)			npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid)		npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)	npcHandler:onCreatureSay(cid, type, msg)	end
function onThink()						npcHandler:onThink()						end

function thinkCallback(cid)
	local rand = math.random(250)

	if thinkMsg[rand] then
		npcHandler:say(thinkMsg[rand])
	end

	return true
end

npcHandler:setCallback(CALLBACK_ONTHINK, thinkCallback)
npcHandler:addModule(FocusModule:new())