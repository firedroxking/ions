local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)

NpcSystem.parseParameters(npcHandler)

function onCreatureAppear(playerID)
	npcHandler:onCreatureAppear(playerID)
end

function onCreatureDisappear(playerID)
	npcHandler:onCreatureDisappear(playerID)
end

function onCreatureSay(playerID, type, message)
	if(message == 'cura') then
		quentinStatusHeal(playerID)
	end

	npcHandler:onCreatureSay(playerID, type, message)
end

function onThink()
	npcHandler:onThink()
end

-- CURA OS STATUS DO PLAYER
function quentinStatusHeal(playerID)
	if(hasCondition(playerID, CONDITION_FIRE)) then
		doRemoveCondition(playerID, CONDITION_FIRE)
	elseif(hasCondition(playerID, CONDITION_POISON)) then
		doRemoveCondition(playerID, CONDITION_POISON)
	elseif(hasCondition(playerID, CONDITION_ENERGY)) then
		doRemoveCondition(playerID, CONDITION_ENERGY)
	else
		npcHandler:say('Voce nem esta tao mal assim, ' .. getCreatureName(playerID) .. '. Eu so posso ajudar quando for uma emergencia.', playerID)
	end
end

npcHandler:addModule(FocusModule:new())