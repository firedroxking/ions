local keywordHandler = KeywordHandler:new()

local npcHandler = NpcHandler:new(keywordHandler)

NpcSystem.parseParameters(npcHandler)



function onCreatureAppear(cid)                  npcHandler:onCreatureAppear(cid)                        end

function onCreatureDisappear(cid)               npcHandler:onCreatureDisappear(cid)                     end

function onCreatureSay(cid, type, msg)          npcHandler:onCreatureSay(cid, type, msg)                end

function onThink()                              npcHandler:onThink()                                    end



function promotion(cid, message, keywords, parameters, node)

        if(not npcHandler:isFocused(cid)) then

                return false

        end

        

        if isPremium(cid) then

                if(getPlayerPromotionLevel(cid) >= parameters.promotion) then

                        npcHandler:say('You are already promoted!', cid)

                elseif(getPlayerLevel(cid) < parameters.level) then

                        npcHandler:say('I am sorry, but I can only promote you once you have reached level ' .. parameters.level .. '.', cid)

                elseif(not doPlayerRemoveMoney(cid, parameters.cost)) then

                        npcHandler:say('You do not have enough money!', cid)

                else

                        setPlayerPromotionLevel(cid, parameters.promotion)

                        npcHandler:say(parameters.text, cid)

                end

        else

                npcHandler:say("You need a premium account in order to get promoted.", cid)

        end

        

        npcHandler:resetNpc()

        return true

end



local node1 = keywordHandler:addKeyword({'promotion'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'You need Level 50 to second promotion.'})

        node1:addChildKeyword({'yes'}, promotion, {npcHandler = npcHandler, cost = 20000, level = 50, promotion = 2, text = 'Gz Man You are Promoted'})

        node1:addChildKeyword({'no'}, promotion, {npcHandler = npcHandler, onlyFocus = true, text = 'Alright then, come back when you are ready.', reset = true})



npcHandler:addModule(FocusModule:new())