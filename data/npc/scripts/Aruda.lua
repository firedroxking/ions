local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)         npcHandler:onCreatureAppear(cid)         end
function onCreatureDisappear(cid)      npcHandler:onCreatureDisappear(cid)      end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink()                     npcHandler:onThink()                     end

function creatureSayCallback(cid, type, msg)
	if(not npcHandler:isFocused(cid)) then
		return false
	end

	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
	
	if msgcontains(msg, "horas") then
		-- QUANTIDADE DE RELOGIOS QUE O PLAYER CARREGA
		local watch = getPlayerItemCount(cid, 2036)

		if (watch >= 1) then
			doPlayerRemoveItem(cid, 2036, 1)
			npcHandler:say("Belo relógio, mas não seja rude, a hora não é importante, estando comigo.", cid)
		else
			npcHandler:say("Não possuo relógio, não sei que horas são.", cid)
		end
	elseif msgcontains(msg, "ladra") then
		selfSay("Me desculpe, estou com pressa, adeus!", cid)
		npcHandler:releaseFocus(cid)
	else 
		-- QUANTIDADE DE DINHEIRO QUE O PLAYER CARREGA
		local moneyAmount = getPlayerMoney(cid)

		if (moneyAmount >= 10) then
			npcHandler:say("Oh, desculpe, eu me distrai, o que você disse?", cid)
			doPlayerRemoveMoney(cid, math.random(1, 10))
		elseif (moneyAmount > 0 and moneyAmount < 10) then
			npcHandler:say("Oh, desculpe, eu me distrai um pouco, o que você disse?", cid)
			doPlayerRemoveMoney(cid, moneyAmount)
		else
			npcHandler:say("Oh desculpe, acabei de me lembra que tenho algo a fazer, tchau!", cid)
			npcHandler:releaseFocus(cid)
		end
	end
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new()) 