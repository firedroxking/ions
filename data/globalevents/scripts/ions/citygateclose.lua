local GATES =
{
	-- COULLIST SOUTH-WEST GATE
	{ ["UNDER"] = {x = 500, y = 68, z = 7}, ["ABOVE"] = {x = 500, y = 68, z = 6}, ["GATE"] = 1547 },
	{ ["UNDER"] = {x = 501, y = 68, z = 7}, ["ABOVE"] = {x = 501, y = 68, z = 6}, ["GATE"] = 1547 },
	{ ["UNDER"] = {x = 502, y = 68, z = 7}, ["ABOVE"] = {x = 502, y = 68, z = 6}, ["GATE"] = 1547 },
	{ ["UNDER"] = {x = 503, y = 68, z = 7}, ["ABOVE"] = {x = 503, y = 68, z = 6}, ["GATE"] = 1547 },
	{ ["UNDER"] = {x = 504, y = 68, z = 7}, ["ABOVE"] = {x = 504, y = 68, z = 6}, ["GATE"] = 1547 },
	{ ["UNDER"] = {x = 505, y = 68, z = 7}, ["ABOVE"] = {x = 505, y = 68, z = 6}, ["GATE"] = 1547 },
	{ ["UNDER"] = {x = 506, y = 68, z = 7}, ["ABOVE"] = {x = 506, y = 68, z = 6}, ["GATE"] = 1547 },
	
	-- COULLIST NORTH-EAST GATE
	{ ["UNDER"] = {x = 544, y = 40, z = 7}, ["GATE"] = 1547 },
	{ ["UNDER"] = {x = 545, y = 40, z = 7}, ["GATE"] = 1547 },
	{ ["UNDER"] = {x = 546, y = 40, z = 7}, ["GATE"] = 1547 },
	{ ["UNDER"] = {x = 547, y = 40, z = 7}, ["GATE"] = 1547 },
	{ ["UNDER"] = {x = 548, y = 40, z = 7}, ["GATE"] = 1547 }	
}

-- GLOBALEVENTS.XML
function onThink()
	local idMechanism = 9531
	local hour        = getHour()
	local minute      	= getMinute()

	-- ABRE AS 05H FECHA AS 20H
	if((hour == 5 or hour == 20) and minute == 0) then
		for index=1, table.getn(GATES), 1 do
	    	local gateUnder = GATES[index]["UNDER"]
	    	local gateAbove = GATES[index]["ABOVE"]
	    	local gateItem  = GATES[index]["GATE"]    	

	    	-- ABRE O PORTAO
	    	if(hour == 5 and gateAbove == nil) then
		    	-- QUANDO O PORTAO NAO TIVER A PARTE DE CIMA
		    	if(getTileItemById(gateUnder, gateItem).uid == 0) then
			    	doCreateItem(gateItem, 1, gateUnder)
			    elseif(getTileItemById(gateUnder, gateItem).uid ~= 0) then
			    	doRemoveItem(getTileItemById(gateUnder, gateItem).uid, 1)
			    end
			end

			-- FECHA O PORTAO
			if(hour == 20 and gateAbove ~= nil) then
			    -- QUANDO O PORTAO TIVER A PARTE DE CIMA
		    	if(getTileItemById(gateAbove, gateItem).uid ~= 0) then
			    	doRemoveItem(getTileItemById(gateAbove, gateItem).uid, 1)
			    	doCreateItem(gateItem, 1, gateUnder)
			    elseif(getTileItemById(gateUnder, gateItem).uid ~= 0) then
			    	doRemoveItem(getTileItemById(gateUnder, gateItem).uid, 1)
			    	doCreateItem(gateItem, 1, gateAbove)
			    end
			end
	    end
	end

    return true
end

-- MINUTO ATUAL VEZES 60 MAIS SEGUNDO ATUAL DIVIDIDO POR 150
function getTibianTime()
	return (os.date('%M') * 60 + os.date('%S')) / 150
end

-- RETORNA A HORA, BASEADO NO HORARIO TIBIANO
function getHour()
	return math.floor(getTibianTime())
end

-- RETORNA O MINUTO, BASEADO NO HORARIO TIBIANO
function getMinute()
	return math.floor(60 * (getTibianTime() - getHour()))
end