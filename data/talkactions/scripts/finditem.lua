function onSay(cid, words, param)
	if param == '' or tonumber(param) then
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "/find item name")
		return true
	end
	local item_id = getItemIdByName(tostring(param), false) 
	if not item_id then
		doPlayerSendCancel(cid, "This item does not exist.") return true
	end
	local str, player_depotitems, players_items, tile_items = "",{},{},{}
	local dp = db.getResult("SELECT `player_id`, `count` FROM `player_depotitems` WHERE `itemtype` = "..item_id),{}
	if (dp:getID() ~= -1) then
		repeat
			player_depotitems[#player_depotitems+1] = {dp:getDataInt("player_id"), dp:getDataInt("count") }
		until not(dp:next())
		dp:free()
	end
	local pi = db.getResult("SELECT `player_id`, `count` FROM `player_items` WHERE `itemtype` = "..item_id),{}
	if (pi:getID() ~= -1) then
		repeat
			players_items[#players_items+1] = {pi:getDataInt("player_id"), pi:getDataInt("count") }
		until not(pi:next())
		pi:free()
	end
	local hi = db.getResult("SELECT `tile_id`, `count` FROM `tile_items` WHERE `itemtype` = "..item_id),{}
	if (hi:getID() ~= -1) then
		repeat
			local tile = db.getResult("SELECT `house_id`, `x`, `y`, `z` FROM `tiles` WHERE `id` = "..hi:getDataInt("tile_id")),{}
			tile_items[#tile_items+1] = {tile:getDataInt("house_id"),tile:getDataInt("x"),tile:getDataInt("y"),tile:getDataInt("z")}
		until not(hi:next())
		hi:free()
	end	 
	if #player_depotitems > 0 then
		str = str .. "#DEPOT ITEMS#\nQuantidade - Jogador\n"
		for i = 1, table.maxn(player_depotitems) do
			str = str .. player_depotitems[i][2] .. ' ' .. getPlayerNameByGUID(player_depotitems[i][1]) ..' \n'
		end
	end
	if #players_items > 0 then
		str = str .. (str ~= "" and "--------------//-------------\n\n#PLAYER ITEMS#\nQuantidade - Jogador\n" or "#PLAYER ITEMS#\nQuantidade - Jogador\n") 
		for i = 1, table.maxn(players_items) do
			str = str .. players_items[i][2] .. ' ' .. getPlayerNameByGUID(players_items[i][1]) ..' \n'
		end
	end
	if #tile_items > 0 then
		str = str .. (str ~= "" and "--------------//-------------\n\n#TILE ITEMS#\nHouse ID - Tile Position\n" or "#TILE ITEMS#\nHouse ID -Tile Position\n") 
		for i = 1, table.maxn(tile_items) do
			str = str .. tile_items[i][1] .. ' - {x = ' .. tile_items[i][2] ..', y = ' .. tile_items[i][3] ..', z = ' .. tile_items[i][4] ..'} \n'
		end
	end
	return doShowTextDialog(cid,item_id, str)
end