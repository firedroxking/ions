function onSay(cid, words, param) 
    if getPlayerTown(cid) == 24 then 
        doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You cannot withdraw money being in Rookgaard.") 
        return 
    end 
     
    if isPlayer(cid) == TRUE and param ~= "" then 
                if getPlayerBalance(cid) >= param then 
                    doPlayerSetBalance(cid, getPlayerBalance(cid) - param) 
                    doPlayerAddMoney(cid, param) 
                    doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "Here you are, ".. param .." gold.") 
                else 
                    doPlayerSendCancel(cid, "You do not have enough money.") 
                    doSendMagicEffect(getPlayerPosition(cid), CONST_ME_POFF) 
                end 
        else 
            doPlayerSendCancel(cid, "You can only use this command in PZ.") 
            doSendMagicEffect(getPlayerPosition(cid), CONST_ME_POFF) 
       
    end 
end