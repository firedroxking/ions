function onSay(cid, words, param, channel)

local config = {
[1] = {level = 20, valor = 20000},
[2] = {level = 20, valor = 20000},
[3] = {level = 20, valor = 20000},
[4] = {level = 20, valor = 20000},
vipsconfig = {onlyvips = "no", storagevip = 1020}, --  Apenas players vips "yes" ou "no" e o storage da vip.
onlypremmy = "no" -- Apenas players premium accounts "yes" or "no".
}

if config.onlypremmy == "yes" and not isPremium(cid) then
return doPlayerSendTextMessage(cid, 23, "Sorry, only premium players.")
end
if config.vipsconfig.onlyvips == "yes" and getPlayerStorageValue(cid, config.vipsconfig.storagevip) - os.time() <= 0 then
return doPlayerSendTextMessage(cid, 23, "Sorry, only vips players.")
end
if config[getPlayerVocation(cid)] then
if getPlayerLevel(cid) >= config[getPlayerVocation(cid)].level then
if doPlayerRemoveMoney(cid, config[getPlayerVocation(cid)].valor) then
setPlayerPromotionLevel(cid, 1)
doPlayerSendTextMessage(cid, 22, "Congratulations, you have been successfully promoted!")
doSendMagicEffect(cid, 14)
else
doPlayerSendTextMessage(cid, 23, "You need "..config[getPlayerVocation(cid)].valor.." gold coins to promote.")
end
else
doPlayerSendTextMessage(cid, 23, "You need level "..config[getPlayerVocation(cid)].level.." to promote.")
end
else
doPlayerSendTextMessage(cid, 23, "Sorry, you already promoted.")
end
return TRUE
end