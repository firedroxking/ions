function onSay(cid, words, param) 
    if getPlayerTown(cid) == 24 then --------- ID DA CIDADE
        doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You cannot deposit money being in Rookgaard.") 
        return 
    end 
     
    if isPlayer(cid) == TRUE and param ~= "" then 
        if getTilePzInfo(getPlayerPosition(cid)) == TRUE then 
                if    doPlayerRemoveMoney(cid, param) == TRUE then 
                    doPlayerSetBalance(cid, param) 
                    doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "You have added the amount of ".. param .." gold to your balance. You can withdraw your money anytime you want to.") 
                else 
                    doPlayerSendCancel(cid, "You do not have enough money.") 
                    doSendMagicEffect(getPlayerPosition(cid), CONST_ME_POFF) 
                end 
        else 
            doPlayerSendCancel(cid, "You can only use this command in PZ.") 
            doSendMagicEffect(getPlayerPosition(cid), CONST_ME_POFF) 
        end 
    end 
end