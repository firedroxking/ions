--[[( Marcryzius )]]-- 
local MSCB = MESSAGE_STATUS_CONSOLE_BLUE 
 
function dip (cid,item,quant) 
local uid = doCreateItemEx(item,quant) 
local get = getThing(uid) 
   if(get.type > 0)then 
      if(doPlayerAddItemEx(cid,uid) == false)then 
         return false,'The player can not receive the item {'..getItemNameById(item)..'} because not have space.' 
      end 
   elseif(uid and getItemNameById(item))then 
      for _= 1,quant do 
         if(doPlayerAddItem(cid,item,1,false) == false)then 
            return false 
         end 
      end 
   else 
      return false,'Item not found!' 
   end 
   return true 
end 
function expremiar (cid,item,quant) 
   if(doPlayerRemoveItem(cid,item,quant) == false)then 
      return false 
   end 
   return true 
end 
 
function onSay(cid, words, param) 
if(param == "") then return true,doPlayerSendTextMessage(cid,MSCB, "Command param required.")end 
local C,t = {},string.explode(param, ",") 
C.ret = RETURNVALUE_NOERROR 
C.tmp = getCreaturePosition(cid) 
C.amount = (t[3] == nil) and 1 or tonumber (t[3]) 
C.nameitem = (tonumber(t[2]) == nil) and t[2] or getItemNameById(t[2]) 
C.id = (tonumber(t[2]) == nil) and getItemIdByName(t[2]) or tonumber(t[2]) 
C.pid = getPlayerByNameWildcard(t[1]) 
if(C.id == LUA_ERROR)then return true,doPlayerSendTextMessage(cid,MSCB, "Item wich such name or id does not exists.")end 
if(C.pid == 0 or (isPlayerGhost(C.pid) == true and getPlayerAccess(C.pid) > getPlayerAccess(cid))) then 
   return true,doPlayerSendTextMessage(cid,MSCB, "Player " .. param .. " is not currently online.") 
end 
   if(words == '/expremiar')then 
      if(expremiar(C.pid,C.id,C.amount) == false)then 
         doPlayerSendTextMessage(cid,MSCB, "the player ("..getCreatureName(C.pid)..") does not have that item ("..C.nameitem..") or the quantity of them ("..C.amount..").") 
      else 
         doPlayerSendTextMessage(cid,MSCB,"Voce removeu do jogador ("..getCreatureName(C.pid)..") o item ("..C.nameitem..") com a Id ("..C.id..") de quantidade "..C.amount..".") 
      end 
   else 
   local bool,msg = dip(C.pid,C.id,C.amount) 
      if(bool == false)then 
         doPlayerSendTextMessage(cid,MSCB,msg) 
      else 
         doPlayerSendTextMessage(cid,MSCB, "The player ("..getCreatureName(C.pid)..") received the item, ("..C.nameitem..") and Id ("..C.id..") in the amount of ("..C.amount..").") 
         doPlayerSendTextMessage(C.pid,MSCB, "You have received the item, ("..C.nameitem..") in the amount of ("..C.amount..") to the ("..getPlayerGroupName(cid)..") ("..getCreatureName(cid)..").") 
      end 
   end 
   return true 
end 