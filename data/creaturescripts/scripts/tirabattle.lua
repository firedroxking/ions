function onThink(cid)
    if isPlayer(cid) and getTileInfo(getThingPos(cid)).protection and getCreatureCondition(cid, CONDITION_INFIGHT) then
        doRemoveCondition(cid, CONDITION_INFIGHT)
    end

    return true
end