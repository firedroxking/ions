function onAttack(cid, target)

local storage = 19387
local tempo = 1*102 ----(1 minuto)
local minutes = 3
if isMonster(target) and getCreatureName(target):lower() == 'training' then
doPlayerSetStorageValue(cid, storage, getPlayerStorageValue(cid, storage)+1)
if getPlayerStorageValue(cid, storage) >= tempo then
doPlayerAddStamina(cid, minutes)
doPlayerSetStorageValue(cid, storage, 0)
end
end
return true
end